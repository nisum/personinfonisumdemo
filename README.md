# Demo Nisum Test Persona Info

API RestFul CRUDTEST microservices realizada en Java 8 con el Framework SpringBoot
Database: h2 en memoria (Archivo Data.sql con la estrucutura de datos necesaria para la ejecucion automatica)
application.properties, con la info de Conexio hacia la DB
TestUnitario: se creatn test de integracion e implementacion

Al momento de iniciar el microservicios las instancia de H2 Database se ejecuta autamaticamente, sin ningun registro 
por defecto

Es necesario crear los recursos en este caso persona mediante el POSTMAN, se adjuntara mediante correo el
la collection del mismo


# Documentacion de la API CRUSTEST mediante Swagger

Se implementa a la API el plugins de Swagger para la documentacion del mismo y el entendimiento de los serivicios
a cosumir o ejecutar, creando una pequeña Documentacion automatizada, se debe levantar el microservicios para poder
consultar la documentacion mediante la siguiente URL:

http://localhost:8080/swagger-ui.html#/persona-info-controller

## EndPoint a consumir
Los metodos de la API son los siguientes
```
http://localhost:8080/persons (POST)
``` 
Crear una persona con su info 

```
http://localhost:8080/persons (GET)
``` 
Regresa una lista de las personas creadas

```
http://localhost:8080/persons/:id (GET)
``` 
Regresa los datos de una persona

```
http://localhost:8080/persons/:id (PUT)
``` 
Actualiza todos los datos de una persona

```
http://localhost:8080/persons/:id (PATCH)
``` 
Actualiza uno o varios datos de una persona

```
http://localhost:8080/persons/:id (DELETE)
``` 
Borra todos los datos de una persona