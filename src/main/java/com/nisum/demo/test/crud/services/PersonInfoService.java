package com.nisum.demo.test.crud.services;

import com.nisum.demo.test.crud.entity.PersonsInfoEntity;

import java.util.List;
import java.util.Optional;


public interface PersonInfoService {

    PersonsInfoEntity createPerson(PersonsInfoEntity body) throws Exception;

    Optional<PersonsInfoEntity> getPersonById(Integer id) throws Exception;

    PersonsInfoEntity updatePartPerson(Integer id,PersonsInfoEntity body)  throws Exception;

    PersonsInfoEntity updateAllPerson(Integer id,PersonsInfoEntity body)  throws Exception;

    void deletePersona(Integer id) throws Exception;

    List<PersonsInfoEntity> getAllPerson() throws Exception;
}
