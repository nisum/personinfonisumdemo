package com.nisum.demo.test.crud.mapperRepository;


import com.nisum.demo.test.crud.entity.PersonsInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonsInfoMapperRepository extends JpaRepository<PersonsInfoEntity, Integer> {

    List<PersonsInfoEntity> findByName(String name);

}
