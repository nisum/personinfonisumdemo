package com.nisum.demo.test.crud.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nisum.demo.test.crud.entity.PersonsInfoEntity;
import com.nisum.demo.test.crud.services.PersonInfoService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


@Api(value = "Nisum Demo CRUD")
@RestController
public class PersonInfoController {

    @Autowired
    private PersonInfoService personInfoService;

    /**
     * @param body
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "Created a Person inside the API", response = PersonsInfoEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Data of Person Save success", response = PersonsInfoEntity.class)
            ,
            @ApiResponse(code = 404, message = "Dont Find resource")
            ,
            @ApiResponse(code = 500, message = "Internal Error of Server")
    })
    @PostMapping(value = "/persons", produces = {"application/json"})
    public ResponseEntity<PersonsInfoEntity> createPerson(
            @ApiParam(value = "Param for crate persons", required = true)
            @Valid @RequestBody PersonsInfoEntity body) throws Exception {

        return new ResponseEntity<>(personInfoService.createPerson(body), HttpStatus.OK);

    }

    /**
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "Search and List a Person inside the API", response = PersonsInfoEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List Data of Person Save success", response = PersonsInfoEntity.class)
            ,
            @ApiResponse(code = 404, message = "Dont Find resource")
            ,
            @ApiResponse(code = 500, message = "Internal Error of Server")
    })
    @GetMapping(value = "/persons", produces = {"application/json"})
    public ResponseEntity<List<PersonsInfoEntity>> getAllPerson()
            throws Exception {

        return new ResponseEntity<>(personInfoService.getAllPerson(), HttpStatus.OK);

    }

    /**
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "Get data of a one Person inside the API", response = PersonsInfoEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Get Data of Person show success", response = PersonsInfoEntity.class)
            ,
            @ApiResponse(code = 404, message = "Dont Find resource")
            ,
            @ApiResponse(code = 500, message = "Internal Error of Server")
    })
    @GetMapping(value = "/persons/{id}", produces = {"application/json"})
    public ResponseEntity<Optional<PersonsInfoEntity>> getPersonById(
            @ApiParam(value = "Id de la persona", required = true)
            @Valid @PathVariable("id") @NumberFormat Integer id) throws Exception {

        return new ResponseEntity<Optional<PersonsInfoEntity>>(personInfoService.getPersonById(id), HttpStatus.OK);

    }

    /**
     * @param id
     * @param body
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "Update someone data a Person inside the API", response = PersonsInfoEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Update someone Data of Person Save success", response = PersonsInfoEntity.class)
            ,
            @ApiResponse(code = 404, message = "Dont Find resource")
            ,
            @ApiResponse(code = 500, message = "Internal Error of Server")
    })
    @PatchMapping(value = "/persons/{id}", produces = {"application/json"})
    public ResponseEntity<PersonsInfoEntity> updatePartPerson(
            @ApiParam(value = "Update someone Data Of Persons", required = true)
            @PathVariable("id") Integer id,
            @Valid @RequestBody PersonsInfoEntity body) throws Exception {

        return new ResponseEntity<>(personInfoService.updatePartPerson(id, body), HttpStatus.OK);

    }

    /**
     * @param id
     * @param body
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "Update all data a Person inside the API", response = PersonsInfoEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Update all Data of Person Save success", response = PersonsInfoEntity.class)
            ,
            @ApiResponse(code = 404, message = "Dont Find resource")
            ,
            @ApiResponse(code = 500, message = "Internal Error of Server")
    })
    @PutMapping(value = "/persons/{id}", produces = {"application/json"})
    public ResponseEntity<PersonsInfoEntity> updateAllPerson(
            @ApiParam(value = "Update all Data Of Persons", required = true)
            @PathVariable("id") Integer id,
            @Valid @RequestBody PersonsInfoEntity body) throws Exception {

        return new ResponseEntity<>(personInfoService.updateAllPerson(id, body), HttpStatus.OK);

    }

    /**
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "Delete All Data a Person inside the API", response = PersonsInfoEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Delete All Data Data of Person Save success", response = PersonsInfoEntity.class)
            ,
            @ApiResponse(code = 404, message = "Dont Find resource")
            ,
            @ApiResponse(code = 500, message = "Internal Error of Server")
    })
    @DeleteMapping(value = "/persons/{id}", produces = {"application/json"})
    public ResponseEntity<String> deletePersona(
            @ApiParam(value = "Delete All Data of Person", required = true)
            @PathVariable("id") Integer id) throws Exception {

        personInfoService.deletePersona(id);
        ObjectMapper objectMapper = new ObjectMapper();
        return new ResponseEntity<>(objectMapper.writeValueAsString("OK -> Person Deleted With ID = " + id), HttpStatus.OK);

    }


}
