package com.nisum.demo.test.crud.services.imple;

import com.nisum.demo.test.crud.entity.PersonsInfoEntity;
import com.nisum.demo.test.crud.mapperRepository.PersonsInfoMapperRepository;
import com.nisum.demo.test.crud.services.PersonInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Service
public class PersonInfoImple implements PersonInfoService {

    private final static Logger logger = Logger.getLogger(String.valueOf(PersonInfoImple.class));

    @Autowired
    PersonsInfoMapperRepository personsInfoMapperRepository;

    @Override
    public PersonsInfoEntity createPerson(PersonsInfoEntity body) throws Exception {

        return personsInfoMapperRepository.save(body);
    }

    @Override
    public Optional<PersonsInfoEntity> getPersonById(Integer id) throws Exception {

        return personsInfoMapperRepository.findById(id);

    }

    @Override
    public PersonsInfoEntity updatePartPerson(Integer id, PersonsInfoEntity body) throws Exception {

        return personsInfoMapperRepository.save(body);
    }

    @Override
    public PersonsInfoEntity updateAllPerson(Integer id, PersonsInfoEntity body) throws Exception {

        return personsInfoMapperRepository.save(body);
    }

    @Override
    public void deletePersona(Integer id) throws Exception {

        personsInfoMapperRepository.deleteById(id);
    }

    @Override
    public List<PersonsInfoEntity> getAllPerson() throws Exception {
        return personsInfoMapperRepository.findAll();
    }


}
