package com.nisum.demo.test.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudNisumApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrudNisumApplication.class, args);
    }

}
