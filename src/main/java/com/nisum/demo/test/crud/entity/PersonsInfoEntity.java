package com.nisum.demo.test.crud.entity;


import com.nisum.demo.test.crud.enums.EnumsHairColors;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity(name = "PERSONS")
public class PersonsInfoEntity implements Serializable {


    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Integer id;

    @NotBlank
    private String name;

    @NotBlank
    private String lastName;

    @NotBlank
    private String address;

    @NumberFormat
    private Integer simplePhoneNumber;

    @Enumerated(EnumType.STRING)
    private EnumsHairColors hairColour;

    public PersonsInfoEntity(){
        super();
    }


    /**
     * @param name
     * @param lastName
     * @param address
     * @param simplePhoneNumber
     * @param hairColour
     */
    public PersonsInfoEntity(@NotBlank String name, @NotBlank String lastName, @NotBlank String address, Integer simplePhoneNumber, EnumsHairColors hairColour) {
        this.name = name;
        this.lastName = lastName;
        this.address = address;
        this.simplePhoneNumber = simplePhoneNumber;
        this.hairColour = hairColour;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getSimplePhoneNumber() {
        return simplePhoneNumber;
    }

    public void setSimplePhoneNumber(Integer simplePhoneNumber) {
        this.simplePhoneNumber = simplePhoneNumber;
    }

    public EnumsHairColors getHairColour() {
        return hairColour;
    }

    public void setHairColour(EnumsHairColors hairColour) {
        this.hairColour = hairColour;
    }

    @Override
    public String toString() {
        return "PersonInfoEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", simplePhoneNumber=" + simplePhoneNumber +
                ", hairColour=" + hairColour +
                '}';
    }
}
