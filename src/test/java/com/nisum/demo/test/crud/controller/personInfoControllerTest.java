package com.nisum.demo.test.crud.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nisum.demo.test.crud.entity.PersonsInfoEntity;
import com.nisum.demo.test.crud.enums.EnumsHairColors;
import com.nisum.demo.test.crud.services.PersonInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonInfoController.class)
public class personInfoControllerTest {

    @MockBean
    private PersonInfoService personInfoService;

    @Autowired
    private MockMvc mvc;

    private PersonsInfoEntity personsInfoEntity;

    /**
     * Test controller get person by id Integration
     * @throws Exception
     */
    @Test
    public void getPersonByIdTest() throws Exception {

        personsInfoEntity = new PersonsInfoEntity(
                "Carlos",
                "Sainz",
                "Mallorca",
                999887766,
                EnumsHairColors.yellow);

        given(personInfoService.getPersonById(1)).willReturn(java.util.Optional.ofNullable(personsInfoEntity));

        mvc.perform(MockMvcRequestBuilders
                .get("/persons/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(new ObjectMapper().writeValueAsString(personsInfoEntity)));


    }
}
