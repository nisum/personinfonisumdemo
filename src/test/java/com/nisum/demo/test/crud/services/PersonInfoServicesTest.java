package com.nisum.demo.test.crud.services;

import com.nisum.demo.test.crud.entity.PersonsInfoEntity;
import com.nisum.demo.test.crud.enums.EnumsHairColors;
import com.nisum.demo.test.crud.mapperRepository.PersonsInfoMapperRepository;
import com.nisum.demo.test.crud.services.imple.PersonInfoImple;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.Optional;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersonInfoServicesTest {

    @Mock
    private PersonsInfoMapperRepository personsInfoMapperRepository;

    @InjectMocks
    private PersonInfoService personInfoService  = new PersonInfoImple();

    /**
     * Test of Method get person by id  Implementation
     * @throws Exception
     */
    @Test
    public void testGetPersonaInfoById() throws Exception {
       Optional <PersonsInfoEntity> personsInfoEntity =
               Optional.of(new PersonsInfoEntity("Juan Manuel",
                       "Fangio",
                       "Argentina",
                       4432211,
                       EnumsHairColors.yellow));

        when(personsInfoMapperRepository.findById(1)).thenReturn(personsInfoEntity);

        assertEquals(personsInfoEntity, personInfoService.getPersonById(1));
    }
}
